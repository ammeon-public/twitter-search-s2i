# Twitter Search S2I Application

This project builds up on the [offical demo](https://github.com/OpenShiftDemos/os-sample-python) app for OpenShift's S2I capability.
The aim of the Twitter Search app is to demonstrate how you can inject secrets/environment variables into the container running the app.
In addition, it showcases a live system with dynamic content - the app consumes Twitter's API according to user input.

## Deployment steps

To deploy this app using the ``oc`` command line tool follow these steps:

### Export your Twitter secrets and other variables

**Twitter secrets**

```
export OAUTH_TOKEN="Access Token"
export OAUTH_TOKEN_SECRET="Access Token Secret"
export CONSUMER_KEY="Consumer Key (API Key)"
export CONSUMER_SECRET="Consumer Secret (API Secret)"
```

**Other variables**

```
export REPO_NAME="twitter-search-s2i"
export PROJECT_NAME="twitter-search"
export PROJECT_DISPLAY_NAME="Twitter Search App Project"
```

### Create a project


```
oc new-project $PROJECT_NAME --display-name="$PROJECT_DISPLAY_NAME"
```


### Run the deployment


```
oc new-app python:2.7~https://gitlab.com/IGeorgiou/twitter-search-s2i.git \
-e OAUTH_TOKEN=$OAUTH_TOKEN \
-e OAUTH_TOKEN_SECRET=$OAUTH_TOKEN_SECRET \
-e CONSUMER_KEY=$CONSUMER_KEY \
-e CONSUMER_SECRET=$CONSUMER_SECRET \
&& oc expose svc/$REPO_NAME \
&& oc scale dc/$REPO_NAME --replicas=2
```

*Wait about 2 mins for the app to deploy*

### Access the app

```
oc get route/$REPO_NAME -o jsonpath='{.spec.Host}'
```

This should return the following url:

`<REPO_NAME>-<PROJECT_NAME>.<CLUSTER_PRECONFIGURED_HOSTNAME>`

Visit that URL to access the app.

### Clean Up

```
oc delete all,sa,templates,secrets,pvc --all -n $PROJECT_NAME
oc delete project $PROJECT_NAME
```