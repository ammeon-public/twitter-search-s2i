from flask import Flask, render_template, request
from nltk.corpus import stopwords
from nltk.tokenize import TweetTokenizer
from nltk import FreqDist
from os import environ
import json
import nltk
import re
import twitter

application = Flask(__name__)
application.config['TEST_VAR'] = environ.get('TEST_VAR')
application.config['OAUTH_TOKEN'] = environ.get('OAUTH_TOKEN')
application.config['OAUTH_TOKEN_SECRET'] = environ.get('OAUTH_TOKEN_SECRET')
application.config['CONSUMER_KEY'] = environ.get('CONSUMER_KEY')
application.config['CONSUMER_SECRET'] = environ.get('CONSUMER_SECRET')

auth = twitter.oauth.OAuth(application.config['OAUTH_TOKEN'], application.config['OAUTH_TOKEN_SECRET'],
                           application.config['CONSUMER_KEY'], application.config['CONSUMER_SECRET'])
twitter_api = twitter.Twitter(auth=auth)

tknzr = TweetTokenizer(preserve_case=False)
special_words = [u'https', u'rt', u'\u2026', u'\u201c', u'\u201d', u'\u2014', u'\u2013', u'\u300d', u'\u2019',
                 u'\uff01', u'\u0e48', u'\u0e31', u'\u0e49', u'\u3001', u'\u0e35', u'\u0e34', u'\ufe0f']
ignored_words = stopwords.words("english") + special_words

# Processing Functions


def extract_top_tokens(tweets, q, filter_re='[#,?,:,.,&,",\',\,,\(,\),/,\-,!]'):
    filter_words = ignored_words
    new_q = q.decode('utf-8')
    new_q = new_q.lower()
    filter_words.append(new_q)
    corpus = []
    for tweet in tweets:
        tokens = tknzr.tokenize(tweet['text'])
        filtered_tokens = [token for token in tokens if token not in filter_words]
        filtered_tokens = [token for token in filtered_tokens if not (
            re.search(filter_re, token))]
        corpus = corpus + filtered_tokens
    fr = FreqDist(corpus)
    top_tokens = fr.most_common()
    top_tokens = top_tokens[:4]
    return top_tokens


def extract_top_tweets(tweets):
    ptweets = []
    for tweet in tweets:
        ptweet = {}
        ptweet['text'] = tweet['text']
        ptweet['id'] = tweet['id']
        ptweet['screen_name'] = tweet['user']['screen_name']
        ptweet['score'] = tweet['favorite_count'] + \
            tweet['retweet_count']+tweet['user']['followers_count']
        ptweets.append(ptweet)
    ptweets = sorted(ptweets, key=lambda k: k['score'], reverse=True)
    toptweets = ptweets[0:10]
    return toptweets

# Routes


@application.route('/')
def index():
    return render_template('index.html')


@application.route('/search')
def search():
    q = request.args.get('q')
    if not q:
        return abort(400)
    count = 100
    lang = "en"
    search_results = twitter_api.search.tweets(
        q=q, count=count, lang=lang, result_type="mixed")
    statuses = search_results['statuses']
    ttweets = extract_top_tweets(statuses)
    twords = extract_top_tokens(statuses, q)
    response = {}
    response["tweets"] = ttweets
    response["words"] = dict(twords)
    return json.dumps(response)


if __name__ == '__main__':
    application.run()
