function Clear(item) {
    var i = document.getElementById(item);
    while (i.firstChild) {
        i.removeChild(i.firstChild);
    }
    var h = document.getElementById("h" + item);
    if (h != null) {
        h.style.visibility = 'hidden';
    }
}

function Handle(e) {
    e.preventDefault();
    var q = document.getElementById('searchbox').value;
    Search(q);
}

function Search(q) {
    if (q == "") {
        alert("Search can't be empty.");
        return false;
    }
    if (q.toLowerCase() == "redhat") {
        alert("Try searching \"red hat\" instead.");
        return false;
    }
    SearchTwitter(q);
}

function SearchTwitter(params) {
    var xmlhttp = new XMLHttpRequest();
    var url = "search";

    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            ShowTopTweets(response["tweets"]);
            ShowTopWords(response["words"]);
        }
    };
    xmlhttp.open("GET", url + "?q=" + params, true);
    xmlhttp.send();
}

function ShowTopTweets(response) {
    Clear("warnings");
    Clear("toptweets");
    if (response === undefined || response.length == 0) {
        var warnings = document.getElementById('warnings');
        var p = document.createElement('p');
        p.innerHTML = "No Tweets found, try a different search word.";
        p.className = "warning";
        warnings.appendChild(p);
    }
    else {
        var listoftweets = document.getElementById('toptweets');
        var h = document.getElementById('htoptweets');
        h.style.visibility = 'visible';
        for (tweet in response) {
            var li = document.createElement('li');
            li.innerHTML = "<span class=\"tweet-displayname\">@" + response[tweet]["screen_name"] + "  - </span>" + response[tweet]["text"];
            li.className = "tweet";
            listoftweets.appendChild(li);
        }
    }
}

function ShowTopWords(response) {
    Clear("warnings");
    Clear("topwords");
    if (response === undefined || Object.keys(response).length === 0) {
        var warnings = document.getElementById('warnings');
        var p = document.createElement('p');
        p.innerHTML = "No Tweets found, try a different search word.";
        p.className = "warning";
        warnings.appendChild(p);
    }
    else {
        var tableofwords = document.getElementById('topwords');
        var h = document.getElementById('htopwords');
        h.style.visibility = 'visible';
        for (var key in response) {
            var td = document.createElement('td');
            td.innerHTML = key + "</br>" + "<span style=\"color:#00DEFF;font-size: 200%;\">" + response[key];
            td.className = "doughnut";
            tableofwords.appendChild(td);
        }
    }
}